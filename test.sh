#!/bin/sh

set -e

# A 16MiB test file
dd if=/dev/urandom of=data.dump ibs=1M count=16

./ddsect.sh info example.map
./ddsect.sh split data.dump example.map
./ddsect.sh join example.map new.dump

md5sum data.dump new.dump

rm -f data.dump new.dump *.bin
