#!/bin/sh
#
# ddsect - dissect (and reassemble) raw data dumps using a memory map file
#
# Copyright (C) 2012  Antonio Ospite <ospite@studenti.unina.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

echo_exec()
{
  echo $@
  $@
}

parse_map_file()
{
  MAP_FILE="$1"

  cat $MAP_FILE | sed -e 's/#.*$//' -e '/^[[:space:]]*$/d' | tr -d ' ' |
  while read line;
  do
    RANGE=$(echo $line | cut -d ':' -f 1)
    START_ADDRESS=$(( $(echo $RANGE | cut -d '-' -f 1) ))
    END_ADDRESS=$(( $(echo $RANGE | cut -d '-' -f 2) - 1))

    NAME=$(echo $line | cut -d ':' -f 2 | sed -e 's/^"//'  -e 's/"$//')

    SIZE=$(($END_ADDRESS - $START_ADDRESS + 1))

    SIZE_KiB=$(($SIZE / 1024))

    echo "${NAME},${START_ADDRESS},${END_ADDRESS},${SIZE},${SIZE_KiB}"
  done
}

print_map_info()
{
  MAP_FILE="$1"

  (
  echo "NAME START_ADDRESS END_ADDRESS SIZE SIZE_KiB"
  parse_map_file $MAP_FILE
  ) | column -s " ," -t
}

split_image()
{
  IMAGE_FILE="$1"
  MAP_FILE="$2"

  IFS=','
  parse_map_file $MAP_FILE |
  while read NAME START_ADDRESS END_ADDRESS SIZE SIZE_KiB;
  do
    echo_exec dd ibs=1 skip=$START_ADDRESS count=$SIZE if="$IMAGE_FILE" of="${NAME}.bin"
  done
}

join_image()
{
  MAP_FILE="$1"
  IMAGE_FILE="$2"

  IFS=','
  parse_map_file $MAP_FILE |
  while read NAME START_ADDRESS END_ADDRESS SIZE SIZE_KiB;
  do
    echo_exec dd obs=1 seek=$START_ADDRESS count=$SIZE conv=notrunc if="${NAME}.bin" of="$IMAGE_FILE"
  done
}

usage()
{
  cat 1>&2 << EOM
usage: $(basename "$0") <OPTION> [ARGS]

Where OPTION and ARGS are:

	info <MAP_FILE>				show details of the map file
	split <INPUT_IMAGE> <MAP_FILE>		split sections as per MAP_FILE
	join <MAP_FILE> <OUTPUT_IMAGE>		join sections as per MAP_FILE
	-h|--help|help				show this help message
EOM
}

check_option_argument()
{
  ARGUMENT="$1"
  DESCRIPTION="$2"
  [ "x$ARGUMENT" != "x" ] || { echo "No $DESCRIPTION passed." 1>&2; usage; exit 1; }
}

check_input_file_argument()
{
  FILE="$1"
  FILE_DESCRIPTION="$2"
  check_option_argument "$FILE" "$FILE_DESCRIPTION"
  [ -r "$FILE" ] || { echo "Invalid $FILE_DESCRIPTION." 1>&2; exit 1; }
}

command -v dd >/dev/null 2>&1 || { echo "$(basename "$0"): command 'dd' is missing." 1>&2 ; exit 1; }

case $1 in
  info)
    MAP_FILE="$2"
    check_input_file_argument "$MAP_FILE" "map file"

    command -v column >/dev/null 2>&1 || { echo "$(basename "$0"): command 'column' is missing." 1>&2 ; exit 1; }

    print_map_info "$MAP_FILE"
    ;;

  split)
    INPUT_IMAGE="$2"
    check_input_file_argument "$INPUT_IMAGE" "input image"

    MAP_FILE="$3"
    check_input_file_argument "$MAP_FILE" "map file"

    split_image "$INPUT_IMAGE" "$MAP_FILE"
    ;;

  join)
    MAP_FILE="$2"
    check_input_file_argument "$MAP_FILE" "map file"

    OUTPUT_IMAGE="$3"
    check_option_argument "$OUTPUT_IMAGE" "output image"

    join_image "$MAP_FILE" "$OUTPUT_IMAGE"
    ;;

  -h|--help|help)
    usage
    exit 0
    ;;

  *)
    echo "Missing or unknown option." 1>&2
    usage
    exit 1;
    ;;
esac
